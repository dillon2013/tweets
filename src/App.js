import React from 'react';
import './App.css';
import TweetList from './components/TweetList';



class App extends React.Component {
  render() {
    return (
      <main className="App">
        <div className="wrapper">
          <TweetList title="Twitter Feed"/>
        </div>
      </main>
    )
  }
}



export default App;
