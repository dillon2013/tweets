import React from 'react';
import { Tweet } from './Tweet';

function TweetList({title, tweets = []}) {
    return (
        <>
            <h1>{title}</h1>
            {tweets.map(tweet => <Tweet {...tweet} key={tweet.id}/>)}
        </>
    )
}

export {
    TweetList
}