import React from 'react';
import './TweetList.css';

function Tweet({image, username, text, timeStamp}) {
    const date = new Date(timeStamp)
    return (
      <div className="tweet">
        <div className="avatar">
            <img src={image} alt={`twitter user ${username}`}/>
        </div>
        <div className="tweet-content">
          <h3>{username}</h3>
          <p>{text}</p>
          <small>{date.toString()}</small>
        </div>
      </div>
    )
  }

  export {
      Tweet
  }