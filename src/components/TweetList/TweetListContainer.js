import React from 'react';
import { TweetList } from './TweetList';

class TweetListContainer extends React.Component {

    static throwResponseError(response) {
        const {status, statusText} = response;
        throw Error(`${status} ${statusText}`)
    }

    state = {
        tweets: [],
        timeStamp: null
    }

    async componentDidMount() {
        await this.resetTweets();
        await this.fetchTweets();
        setInterval(() => this.fetchTweets(), 2000);
    }

    resetTweets = async () => {
        try {
            const URL = 'http://magiclab-twitter-interview.herokuapp.com/dillon-lee/reset';
            const response = await fetch(URL)
            if(!response.ok) TweetListContainer.throwResponseError(response);
            return response.json();
        } catch (error) {
            console.error('RESET TWEETS', error)
        }
    }

    fetchTweets = async () => {
        try {
            const URL = 'http://magiclab-twitter-interview.herokuapp.com/dillon-lee/api';
            const SEARCH_PARAMS = new URLSearchParams({
                count: 3,
                time: this.state.timeStamp || new Date(),
                direction: 1
            });
            const response = await fetch(`${URL}?${SEARCH_PARAMS}`);
            if(!response.ok) TweetListContainer.throwResponseError(response);

            const data = await response.json();
            const tweetsSortedByDate = data.sort((a, b) => a.timeStamp - b.timeStamp)
            const lastTweet = tweetsSortedByDate[data.length - 1];
            const {timeStamp} = lastTweet;

            this.setState((state) => ({
            tweets: [...tweetsSortedByDate, ...state.tweets],
            timeStamp
            }))

        } catch (error) {
            console.error('FETCH TWEETS', error)
        }
    }

    render() {
        return <TweetList {...this.props} tweets={this.state.tweets}/>
    }
}

export default TweetListContainer;